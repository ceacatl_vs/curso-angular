import { Injectable } from "@angular/core";
import { createEffect, Actions, ofType } from "@ngrx/effects";
import * as actions from '../actions/destinos-state.action'
import { HttpClient } from '@angular/common/http';
import { map, mergeMap } from 'rxjs/operators';
import { DestinoViaje } from '../models/destino-viaje.model';

@Injectable()
export class DestinosViajesEffects {
    private appURL: string = '/home';
    
    constructor(
        private action$: Actions,
        private http: HttpClient,
    ) {}

    nuevoAgregado$ = createEffect(() => this.action$.pipe(
        ofType(actions.nuevoDestinoAction),
        mergeMap(action => this.http.get(this.appURL).pipe(
            map((data: DestinoViaje) => {
                return actions.elegidoFavoritoAction({ e: data });
            })
        ))
        )
    );

    
}
